<?php

namespace App\Console;

use App\Console\Commands\ConnectivityTest;
use App\Console\Commands\currentSales;
use App\Console\Commands\GotInvoices;
use App\Console\Commands\HourlySales;
use App\Console\Commands\PricingCloudTransfer;
use App\Console\Commands\PromoMovement;
use App\Console\Commands\RetalixExport;
use App\Console\Commands\ShoppingGenerate;
use App\Console\Commands\SigisUpdate;
use App\Console\Commands\SilFilesDownload;
use App\Console\Commands\SpinsExport;
use App\Console\Commands\StoreDepartmentsExport;
use App\Console\Commands\StoreSales;
use App\Console\Commands\StoreScaleFlags;
use App\Console\Commands\TestEmail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ConnectivityTest::class,
        HourlySales::class,
        TestEmail::class,
        PricingCloudTransfer::class,
        ShoppingGenerate::class,
        SilFilesDownload::class,
        RetalixExport::class,
        GotInvoices::class,
        SigisUpdate::class,
        SpinsExport::class,
        StoreDepartmentsExport::class,
        StoreSales::class,
        StoreScaleFlags::class,
        currentSales::class,
        PromoMovement::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('departments:export')->saturdays();

        $schedule->command('got:invoices')->dailyAt('9:00');

//        $schedule->command('retalix:export')->dailyAt('0:30');

        $schedule->command('silfiles:download')->cron('0 1 * * FRI');

//        $schedule->command('shopping:generate')->dailyAt('5:00');

//        $schedule->command('spins:export')->everyThirtyMinutes();

        $schedule->command('sales:current')->everyThirtyMinutes();

//        $schedule->command('sales:hourly')->dailyAt('16:00');

        $schedule->command('store:sales')->dailyAt('06:00');

//        $schedule->command('store:scaleflags 3501')->fridays()->at('14:30');
//        $schedule->command('store:scaleflags 1006')->fridays()->at('15:30');
//        $schedule->command('store:scaleflags 1205')->fridays()->at('16:30');
//        $schedule->command('store:scaleflags 1201')->fridays()->at('17:30');
//        $schedule->command('store:scaleflags 7957')->fridays()->at('18:30');
//        $schedule->command('store:scaleflags 3713')->fridays()->at('19:30');
//        $schedule->command('store:scaleflags 2701')->fridays()->at('20:30');
//        $schedule->command('store:scaleflags 1230')->fridays()->at('21:30');
//        $schedule->command('store:scaleflags 9515')->fridays()->at('22:30');
//        $schedule->command('store:scaleflags 4424')->fridays()->at('23:30');
//        $schedule->command('store:scaleflags 4150')->saturdays()->at('00:30');

        $schedule->command('promo:movement')->dailyAt('06:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
