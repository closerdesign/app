<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StoreScaleFlags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:scaleflags {store}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $store = $this->argument('store');

        if( DB::connection($store.'mfs1') )
        {
            DB::connection('sara')
                ->table('scale_flags')
                ->where('store', $store)
                ->delete();

            $flags = DB::connection($store.'mfs1')
                ->table('PLU')
                ->where('DSPL_DESCR', '!=', 'Thanx $10 Reward')
                ->where('DSPL_DESCR', '!=', 'EMPLOYEE DISCOUNT')
//                ->where('WGT_SCALE_FG', '=',1)
                ->select(DB::raw($store . ' AS store'), DB::raw('ITM_ID as upc'), DB::raw('WGT_ITM_FG as scale_flag'), DB::raw('DSPL_DESCR as description'))
//                ->take(100)
                ->get();

            $flags = $flags->toArray();

            foreach ($flags as $flag)
            {
                DB::connection('sara')
                    ->table('scale_flags')
                    ->insert([
                        'store' => $store,
                        'upc' => round($flag->upc),
                        'scale_flag' => $flag->scale_flag,
                        'description' => $flag->description
                    ]);
            }

            echo count($flags);
        }
    }
}
