<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class RetalixExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retalix:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = DB::table('item_price')
            ->join('item_master', 'item_price.item_id', '=', 'item_master.item_id')
            ->join('store_table', 'item_price.store_id', '=', 'store_table.store_id')
            ->leftJoin('item_scale_flags', 'item_master.item_id', '=', 'item_scale_flags.item_id')
            ->where(function($q){
                $q->where('ip_end_date', '>=' , date('Y-m-d'))
                    ->orWhere('ip_end_date', null);
            })
            ->where(function($q){
                $q->where('item_price.price_strategy', 1)
                    ->orWhere('item_price.price_strategy', 2)
                    ->orWhere('item_price.price_strategy', 3)
                    ->orWhere('item_price.price_strategy', 4)
                    ->orWhere('item_price.price_strategy', 5)
                    ->orWhere(function($q){
                        $q->where('item_price.price_strategy', 0)
                            ->where('item_price.ad_group', '>=', 111);
                    });
            })
            ->select(
                'item_master.upc_ean',
                'item_master.description',
                'prm_store_number',
                'ip_unit_price',
                'descriptive_size',
                'ip_start_date',
                'ip_end_date',
                'item_price.price_strategy',
                'item_master.store_pos_department',
                'item_master.pos_flags_id',
                DB::raw('(CASE WHEN item_master.pos_flags_id = 3 THEN 1 ELSE 0 END) as scale_flag'),
                'item_price.ad_group',
                'item_price.ip_price_multiple'
            )
            ->get();

        $file = View::make('pricing.export', ['products' => $products]);

        Storage::disk('s3')->put('retalix-pricing/' . date('Ymd') . '/retalix-pricing.csv', $file, 'public');

        echo $products->count();
    }
}
