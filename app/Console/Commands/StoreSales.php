<?php

namespace App\Console\Commands;

use App\Mail\StandardEmailNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class StoreSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sales Import after EOD.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = stores_list();

        $date = date('Y-m-d 00:00:00.000', strtotime('-1 days'));

        foreach ($stores as $store)
        {
            try{

                DB::connection('sales')
                    ->table('sales_data')
                    ->where('sales_date', date('Y-m-d 00:00:00', strtotime($date)))
                    ->where('store_num', $store)
                    ->delete();

                $sales = DB::connection($store.'mfs1')
                    ->table('DAY_DEP_SALES')
                    ->join('DEP', 'DAY_DEP_SALES.STR_HIER_ID', '=', 'DEP.STR_HIER_ID')
                    ->where('DT', $date)
                    ->select('DEP.DEP_DESCR', 'DAY_DEP_SALES.NET_SLS_AMT', 'DAY_DEP_SALES.NET_SLS_QTY')
                    ->get();

                foreach ($sales as $sale)
                {
                    $amount = ($sale->NET_SLS_AMT == null ? 0 : $sale->NET_SLS_AMT);

                    if( $amount > 0 )
                    {
                        DB::connection('sales')
                            ->table('sales_data')
                            ->insert([
                                'store_num' => $store,
                                'dept_name' => rtrim($sale->DEP_DESCR),
                                'dept_sales' => $amount,
                                'sales_date' => $date,
                                'net_sls_qty' => $sale->NET_SLS_QTY,
                            ]);
                    }
                }

            }

            catch (\Exception $e)
            {
                Mail::to('juanc@closerdesign.co')
                    ->cc('bfry@buyforlessok.com')
                    ->send(new StandardEmailNotification($e->getMessage(), '[' . $store . '] Daily Sales Refresh Issue'));

                echo $e->getMessage();
            }
        }
    }
}
