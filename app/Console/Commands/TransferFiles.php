<?php

namespace App\Console\Commands;

use App\Mail\CloudTransferFinished;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class TransferFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Picks up XML files from Retalix XML Folder, send them to Cloud FTP and back them up on S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $files = Storage::disk('xmlsite')->files();

            $matchingFiles = preg_grep('/.xml/', $files);

            $message = "";

            $i = 0;

            foreach ($matchingFiles as $file) {

                $filename = "";

                if (strpos($file, 'CostsPricesToStore') !== false)
                {
                    $folder = substr($file, 18, 5);

                    $filename = public_path() . '/price' . $folder . date('ymdHi') . '.zip';
                }

                if (strpos($file, 'HQtoStore') !== false)
                {
                    $folder = substr($file, 9, 5);

                    $filename = public_path() . '/item' . $folder . date('ymdHi') . '.zip';
                }

                if (strpos($file, 'SVIOtoStore') !== false)
                {
                    $folder = substr($file, 11, 5);

                    $filename = public_path() . '/svio' . $folder . date('ymdHi') . '.zip';
                }

                if ($filename != "") {

                    // Picking Up Files from the Original Folder

                    $content = Storage::disk('xmlsite')->get($file);

                    // Zipping Files

                    $zip = new ZipArchive();

                    $zip->open($filename, ZipArchive::CREATE);

                    $zip->addFromString($file, $content);

                    $zip->close();

                    // Moving Files Into the Cloud Folder

                    $basename = basename($filename);

                    $public_file = file_get_contents(public_path() . '/' . $basename);

                    Storage::disk('cloudsite')
                        ->put($folder . '/' . $basename, $public_file);

                    // Moving Files into the S3 Instance

                    Storage::disk('s3')
                        ->put('retalix-transfer-files/' . $folder . '/' . $basename, $public_file);

                    // Removing Files from the Original Folder

                    Storage::disk('xmlsite')->delete($file);

                    // Removing Files from the Public Folder

                    File::delete(public_path() . '/' . $basename);

                    // Report File into the Message Body

                    $message .= "<li>" . $basename . "</li>";

                    $i++;
                }
            }

            if ($i > 0)
            {
                // Send Notification Email

                Mail::to('jrodriguez@buyforlessok.com')
                    ->cc(
                        [
                            'irodriguez@buyforlessok.com',
                            'jtanner@buyforlessok.com'
                        ]
                    )
                    ->send(new CloudTransferFinished($message));
            }

            return $i;

        }

        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }
}
