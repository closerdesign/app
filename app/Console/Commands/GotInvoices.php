<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class GotInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'got:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Checking for records in Retalix database.');

        $date = date('Y-m-d', strtotime('-1 days'));

        $invoices = DB::table('receiver')
            ->join('receiver_detail', 'receiver_detail.rcv_id', '=', 'receiver.rcv_id')
            ->join('vendor_master', 'vendor_master.v_id', '=', 'receiver.v_id')
            ->join('store_table', 'receiver.store_id', '=', 'store_table.store_id')
            ->where('receiver.rcv_close_flag', 1)
            ->where('receiver.record_status', '<>', 3)
            ->where('receiver_detail.record_status', '<>', 3)
            ->where('vendor_master.record_status', '<>', 3)
            ->where(function($q){
                $q->where('vendor_master.vendor', '0076377   ')
                    ->orWhere('vendor_master.vendor', '0076370   ')
                    ->orWhere('vendor_master.vendor', '0076376   ')
                    ->orWhere('vendor_master.vendor', '0076371   ');
            })
            ->where('receiver.rcv_effective_date', $date . ' 00:00:00.000')
            ->select('rcv_invoice_no', 'receiver.rcv_effective_date', 'vendor_master.vendor', 'store_table.prm_store_number', 'receiver_detail.store_pos_department', 'receiver_detail.rd_upc_ean', 'receiver_detail.description', 'receiver_detail.cases', 'receiver_detail.units', 'receiver_detail.sell_size', 'receiver_detail.vendor_item', 'receiver.rcv_delivery_date')
            ->get();

        if( $invoices->count() > 0 )
        {
            Log::info('Records found. Creating report and sending via FTP and email');

            $file = View::make('got.invoices', ['invoices' => $invoices]);
            $filename = date('Ymd', strtotime($date)) . '-got-invoices.csv';

            Storage::disk('s3')->put('got-invoices/' . $filename, $file, 'public');
            Storage::disk('got')->put($filename, $file);

            Mail::to([
                'ugotjr@gotsystems.net',
                'sward@buyforlessok.com',
                'juanc@closerdesign.co'
            ])
                ->send(new \App\Mail\GotInvoices($file,$filename));
        }

        Log::info('Invoices report has been sent.');
    }
}
