<?php

namespace App\Console\Commands;

use App\Mail\StandardEmailNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class currentSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales:current';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
//        $date = date('Y-m-d', strtotime('-1 days'));

        $stores = stores_list();

        foreach ($stores as $store)
        {
            try{

                DB::connection('sales')
                    ->table('sales_data')
                    ->where('sales_date', date('Y-m-d 00:00:00', strtotime($date)))
                    ->where('store_num', $store)
                    ->delete();

                DB::connection('sales')
                    ->table('cust_data')
                    ->where('sales_date', date('Y-m-d 00:00:00', strtotime($date)))
                    ->where('store_num', $store)
                    ->delete();

                $sales = DB::connection($store.'mfs1')
                    ->select(DB::raw(
                        "
                SELECT DEP_DESCR as DEPARTMENT, NET_SLS_AMT AS SALES, CONVERT(VARCHAR(10)," . $date . ",111) AS DATE, NET_SLS_QTY
                FROM DEP 
                LEFT JOIN CUR_DEP_SALES AS CDS ON DEP.STR_HIER_ID=CDS.STR_HIER_ID"
                    ));

                foreach ($sales as $sale)
                {
                    $amount = ($sale->SALES == null ? 0 : $sale->SALES);

                    if( $amount > 0 )
                    {
                        DB::connection('sales')
                            ->table('sales_data')
                            ->insert([
                                'store_num' => $store,
                                'dept_name' => rtrim($sale->DEPARTMENT),
                                'dept_sales' => $amount,
                                'net_sls_qty' => $sale->NET_SLS_QTY,
                                'sales_date' => $date
                            ]);
                    }
                }

                $customers = DB::connection($store.'mfs1')
                    ->table('CUR_CONTROL')
                    ->select('NBR_OF_CUST')
                    ->first();

                if( $customers->NBR_OF_CUST > 0 )
                {
                    DB::connection('sales')
                        ->table('cust_data')
                        ->insert([
                            'store_num' => $store,
                            'cust_count' => $customers->NBR_OF_CUST,
                            'sales_date' => $date
                        ]);
                }

            }

            catch (\Exception $e)
            {
                Mail::to('juanc@closerdesign.co')
                    ->cc('bfry@buyforlessok.com')
                    ->send(new StandardEmailNotification($e->getMessage(), '['.$store.'] Sales Import Issue'));

                echo $e->getMessage();
            }
        }
    }
}
