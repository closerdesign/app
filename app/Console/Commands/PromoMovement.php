<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PromoMovement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promo:movement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = [
            '3501',
            '1006',
            '1205',
            '1201',
            '7957',
            '3713',
            '2701',
            '1230',
            '9515',
            '4424',
            '4150',
//            '952'
        ];

        foreach ($stores as $store)
        {
            if( DB::connection($store.'mfs1') )
            {
                $promos = DB::connection($store.'mfs1')
                    ->table('MMBR_PROM')
                    ->join('DAY_PROM_SALES', 'DAY_PROM_SALES.PROM_ID', '=', 'MMBR_PROM.MMBR_PROM_ID')
                    ->where('DAY_PROM_SALES.DT', date('Y-m-d 00:00:00.000', strtotime('-1 days')))
                    ->select('MMBR_PROM.MMBR_PROM_ID', 'MMBR_PROM.STRT_DATE', 'MMBR_PROM.END_DATE', 'MMBR_PROM.PROM_DESC', 'DAY_PROM_SALES.PROM_AMT', 'DAY_PROM_SALES.PROM_QTY', 'DAY_PROM_SALES.DT')
                    ->get();

                if( DB::connection('sara') )
                {
                    foreach ( $promos as $promo )
                    {
                        DB::connection('sara')
                            ->table('promo_movements')
                            ->insert([
                                'mmbr_prom_id' => $promo->MMBR_PROM_ID,
                                'strt_date' => $promo->STRT_DATE,
                                'end_date' => $promo->END_DATE,
                                'prom_desc' => $promo->PROM_DESC,
                                'prom_amt' => $promo->PROM_AMT,
                                'prom_qty' => $promo->PROM_QTY,
                                'dt' => $promo->DT,
                                'store' => $store
                            ]);
                    }
                }
            }
        }
    }
}
