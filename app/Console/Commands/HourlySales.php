<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class HourlySales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime('-1 days'));

        foreach (stores_list() as $store)
        {
            if( DB::connection($store.'mfs1') )
            {
                $sales = DB::connection($store.'mfs1')
                    ->table('DAY_DEP_HRLY_PRDCTVTY')
                    ->where('DT', $date)
                    ->select('*')
                    ->get();

                foreach ($sales as $sale)
                {
                    if( DB::connection('sara') )
                    {
                        DB::connection('sara')
                            ->table('hourly_sales')
                            ->insert([
                                'store_id' => $store,
                                'dt' => $sale->DT,
                                'ttime' => $sale->TTIME,
                                'str_hier_id' => $sale->STR_HIER_ID,
                                'net_sls_amt' => $sale->NET_SLS_AMT,
                                'net_sls_qty' => $sale->NET_SLS_QTY,
                                'cust_qty' => $sale->CUST_QTY
                            ]);
                    }
                }
            }
        }
    }
}
