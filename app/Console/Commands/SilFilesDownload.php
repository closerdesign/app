<?php

namespace App\Console\Commands;

use App\Mail\CloudTransferFinished;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SilFilesDownload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'silfiles:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('awg')->allFiles();

        $msg = "<p>Files Download Process has been completed.</p>";

        $msg .= "<ul>";

        $count = 0;

        foreach ($files as $file)
        {
            if (($file == 'AWG94449.SIL') || ($file == 'AWG04449.SIL'))
            {
                $content = Storage::disk('awg')->get($file);

                $filename = explode('.', $file)[0] . "_." . explode('.', $file)[1];

                Storage::disk('hqpm')->put($filename, $content);

                Storage::disk('hqpm')->put('sil-files/' . date('Ymd') . '/' . $filename, $content);

//                Storage::disk('awg')->delete($file);

                $msg .= "<li>" . $file . "</li>";

                $count++;
            }
        }

        $msg .= "</ul>";

        if( $count > 0 )
        {
            Log::info($count . ' SIL Files found in AWG\'s server.');

            Mail::to('vgomez@buyforlessok.com')
                ->cc(['jrodriguez@buyforlessok.com', 'juanc@closerdesign.co'])
                ->send(new CloudTransferFinished($msg));
        }
    }
}
