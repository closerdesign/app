<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SigisUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sigis:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('s3')->files('sigis-file');

        if( count($files) > 0 )
        {
            $file = Storage::disk('s3')->get($files[0]);

            Excel::load($file, function($reader){

                $results = $reader->first();

                dd($results);

            })->get();

//            $file = fopen($url, "r");
//
//            return $file;

//            //Output lines until EOF is reached
//            while(! feof($file)) {
//
//                $line = fgets($file);
//
//                $indicator = null;
//
//                if( $item->change_indicator != 'D' )
//                {
//                    $indicator = '6';
//                }
//
//                $update = DB::table('item_master')
//                    ->where('upc_ean', $item->upc)
//                    ->update([
//                        'POS_Flags_ID' => $indicator
//                    ]);
//
//                if( $update )
//                {
//                    $count++;
//                }
//            }
//
//            fclose($file);
        }
    }
}
