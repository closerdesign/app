<?php

namespace App\Console\Commands;

use App\Mail\CloudTransferFinished;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PricingCloudTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pricing:cloud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Picking up pricing and sending it to cloud FTP.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $msg = "<ul>";

            $count = 0;

            $directories = Storage::disk('ftpsite')->directories();

            foreach ( $directories as $directory )
            {
                $files = Storage::disk('ftpsite')->files($directory);

                foreach ( $files as $file )
                {
                    $contents = Storage::disk('ftpsite')->get($file);

                    Storage::disk('cloudsite')->put($file, $contents);

                    $msg .= '<li>' . $file . '</li>';

                    $count++;
                }

                Storage::disk('ftpsite')->deleteDirectory($directory);
            }

            $msg .= "</ul>";

            if( $count > 0 )
            {
                Mail::to('jrodriguez@buyforlessok.com')
                    ->cc(
                        [
                            'irodriguez@buyforlessok.com',
                            'jtanner@buyforlessok.com',
                            'mdenison@buyforlessok.com',
                            'scanning@buyforlessok.com'
                        ]
                    )
                    ->send( new CloudTransferFinished( $msg ) );
            }
        }

        catch ( \Exception $e )
        {
            return $e->getMessage();

        }
    }
}
