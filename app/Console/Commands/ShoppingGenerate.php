<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ShoppingGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopping:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Online Shopping Files.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = Request::create(route('generate'), 'GET');

        $response = app()->handle($request);

        $responseBody = $response->getContent();

        $this->info('Generate route has been dispatched successfully');
    }
}
