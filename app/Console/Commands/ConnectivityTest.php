<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConnectivityTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connectivity:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tests = [];

        foreach ( stores_list() as $store )
        {
            if( DB::connection($store.'mfs1') )
            {
                $tests[$store] = 'Success';
            }else{
                $tests[$store] = 'Failed';
            }
        }

        echo json_encode($tests);
    }
}
