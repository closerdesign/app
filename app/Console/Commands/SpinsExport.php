<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class SpinsExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spins:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('spins')->files();

        if( count($files) !== 0 )
        {
            $file = $files[0];

//        Storage::disk('spins-s3')->put($file, Storage::disk('spins')->get($file));
            Storage::disk('s3')->put('spins/' . $file, Storage::disk('spins')->get($file));
            Storage::disk('spins-sftp')->put('buyfor_less' . $file, Storage::disk('spins')->get($file));

            Storage::disk('spins')->delete($file);

            echo $file;
        }
    }
}
