<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class CloudTransferFinished extends Mailable
{
    use Queueable, SerializesModels;

    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $file1 = Storage::disk('hqpm')->get('sil-files/' . date('Ymd') . '/AWG04449_.SIL');
        $file2 = Storage::disk('hqpm')->get('sil-files/' . date('Ymd') . '/AWG94449_.SIL');

        return $this
            ->markdown('email.sil-files-download')
            ->subject('[' . date('Y-m-d') . '] SIL Files Process has been completed.')
            ->attachData($file1, 'AWG04449_.txt', [
                'mime' => 'text/csv',
            ])
            ->attachData($file2, 'AWG94449_.txt', [
                'mime' => 'text/csv'
            ]);
    }
}
