<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OnlineShoppingFiles extends Mailable
{
    use Queueable, SerializesModels;

    public $uploads;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($uploads)
    {
        $this->uploads = $uploads;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.online-shopping-files')
            ->subject('Online Shopping Files Generation Report');
    }
}
