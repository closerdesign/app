<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GotInvoices extends Mailable
{
    public $file;
    public $filename;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file, $filename)
    {
        $this->file = $file;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('got.invoices-notification')
            ->subject('Invoices file has been sent to GOT Systems: ' . $this->filename)
            ->attachData($this->file, $this->filename, [
                'mime' => 'text/csv',
            ]);
    }
}
