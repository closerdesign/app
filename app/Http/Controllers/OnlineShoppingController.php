<?php

namespace App\Http\Controllers;

use App\Mail\OnlineShoppingFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class OnlineShoppingController extends Controller
{
    /**
     *  Stores List
     */

    public function stores()
    {
        $stores = DB::select('
            SET NOCOUNT ON;
            SELECT
                cast(rtrim(ltrim(st.prm_store_number)) as varchar(4)) as \'PRM_STORE_NUMBER\'
            from tciinstore.dbo.STORE_TABLE ST
            RIGHT JOIN tciinstore.dbo.LIST_DETAIL LD ON LD.LISTED_ID=ST.STORE_ID
            RIGHT JOIN tciinstore.dbo.LISTS ON LISTS.LIST_ID=LD.LIST_ID AND (LISTS.LIST_NAME=\'Online_Stores_77\' OR LISTS.LIST_NAME=\'MWG_Stores\')
            WHERE st.record_status <> 3 and st.prm_store_number > 0 and st.STORE_STATUS <> 3
        ');

        return $stores;
    }

    /**
     *  Pricing
     */

    public function pricing($store)
    {
        $pricing = DB::select('
            SET NOCOUNT ON;
            SELECT * FROM (
                Select
                    \'SKU\' as \'SKU\',
                    \'BrandName\' as \'BrandName\',
                    \'ProductName\' as \'ProductName\',
                    \'Description\' as \'Description\',
                    \'Size\' as \'Size\',
                    \'UOM\' as \'UOM\',
                    \'DeptCode\' as \'DeptCode\',
                    \'ScaleFlag\' as \'ScaleFlag\',
                    \'HowToSell\' as \'HowToSell\',
                    \'AvgWeight\' as \'AvgWeight\',
                    \'WgtSelector\' as \'WgtSelector\',
                    \'StoreNumber\' as \'StoreNumber\',
                    \'ADFlag\' as \'ADFlag\',
                    \'COOL\' as \'COOL\',
                    \'RegularPrice\' as \'RegularPrice\',
                    \'SalePrice\' as \'SalePrice\',
                    \'SaleQty\' as \'SaleQty\',
                    \'PromoStartDate\' as \'PromoStartDate\',
                    \'PromoEndDate\' as \'PromoEndDate\',
                    \'PromoBuyQty\' as \'PromoBuyQty\',
                    \'PromoGetQty\' as \'PromoGetQty\',
                    \'PromoMinQty\' as \'PromoMinQty\',
                    \'PromoLimitQty\' as \'PromoLimitQty\',
                    \'FamilySaleFlag\' as \'FamilySaleFlag\',
                    \'FamilySaleCode\' as \'FamilySaleCode\',
                    \'FamilySaleDesc\' as \'FamilySaleDesc\',
                    \'TaxPct\' as \'TaxPct\',
                    \'BottleDeposit\' as \'BottleDeposit\',
                    \'PromoTag\' as \'PromoTag\',
                    \'Category\' as \'Category\',
                    \'SubBrand\' as \'SubBrand\',
                    \'FullProdDesc\' as \'FullProdDesc\',
                    \'Movement\' as \'Movement\'
                UNION
                SELECT
                    SKU				,
                    BrandName		,
                    ProductName		,
                    Description		,
                    SIZE			,
                    UOM				,
                    DeptCode		,
                    ScaleFlag		,
                    HowToSell		,
                    AvgWeight		,
                    WgtSelector		,
                    StoreNumber		,
                    ADFlag			,
                    COOL			,
                    RegularPrice	,
                    SalePrice		,
                    SaleQty			,
                    PromoStartDate	,
                    PromoEndDate	,
                    PromoBuyQty		,
                    PromoGetQty		,
                    PromoMinQty		,
                    PromoLimitQty	,
                    FamilySaleFlag	,
                    FamilySaleCode	,
                    FamilySaleDesc	,
                    TaxPct			,
                    BottleDeposit	,
                    PromoTag		,
                    Category		,
                    SubBrand		,
                    FullProdDesc	,
                    Movement
                FROM MWG_ITEM_PRICE_FILE
                WHERE StoreNumber = ' . $store . '
            ) data
            WHERE data.RegularPrice <> \'\'
            ORDER BY data.StoreNumber DESC,data.SKU ASC');

        $file = View::make('online-shopping.pricing', ['pricing' => $pricing]);

        $file = (string) $file;

        try
        {
            $filename = date('Y-m-d_H-i') . '_ONLINESHOP_' . $store . '.dat';

            Storage::disk('sara')->put('uploads/' . $filename, $file, 'public');
            Storage::disk('s3')->put('online-shopping-files/' . $filename, $file, 'public');

            return ['count' => count($pricing), 'filename' => $filename];
        }

        catch (\Exception $e)
        {
            return $e->getMessage();
        }


    }

    /**
     *  Generate
     */

    public function generate()
    {
        $uploads = [];

        foreach ( $this->stores() as $store )
        {
            array_push($uploads, [
                'store' => $store->PRM_STORE_NUMBER,
                'data' => $this->pricing( $store->PRM_STORE_NUMBER )
            ]);
        }

        Mail::to('rmolder@buyforlessok.com')
            ->cc('juanc@closerdesign.co')
            ->send(new OnlineShoppingFiles($uploads));

        dd($uploads);
    }
}
