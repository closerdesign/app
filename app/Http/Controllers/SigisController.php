<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SigisController extends Controller
{
    /**
     *  Form
     */

    public function form()
    {
        return view('sigis.form');
    }

    /**
     *  File Process
     */

    public function process(Request $request)
    {
        $file = $request->file('file')->getRealPath();

        $data = array_map('str_getcsv', file($file));

        dd($data);

        $count = 0;

        foreach ($data as $i)
        {
            if( DB::table('item_master')
                    ->whereRaw("Isnumeric(item_master.upc_ean) = 1")
                    ->count() > 0)
            {
                $count++;
            }
        }

        return $count;

//        Replace(Ltrim(Replace(LEFT(IM.upc_ean,
//            Len(IM.upc_ean) - 1), '0', ' ')), ' ', '0')
    }
}
