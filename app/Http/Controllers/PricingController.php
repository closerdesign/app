<?php

namespace App\Http\Controllers;

use App\Mail\CloudTransferFinished;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PricingController extends Controller
{
    /**
     *  Daily Transfer to Cloud Site
     *
     *
    */

    public function cloud_transfer()
    {
        try
        {
            $msg = "123";

            $directories = Storage::disk('ftpsite')->directories();

            foreach ( $directories as $directory )
            {
                $files = Storage::disk('ftpsite')->files($directory);

                foreach ( $files as $file )
                {
                    $contents = Storage::disk('ftpsite')->get($file);

                    Storage::disk('cloudsite')->put('test/' . $file, $contents);

                    $msg .= '<br />' . $directory . '/' . $file;
                }
            }

            Mail::to('jrodriguez@buyforlessok.com')->send(new CloudTransferFinished($msg));

            return $msg;
        }

        catch ( \Exception $e )
        {
            return $e->getMessage();

        }
    }

    /**
     *  Pricing Export
     */

    public function export()
    {
        $products = DB::table('item_price')
            ->join('item_master', 'item_price.item_id', '=', 'item_master.item_id')
            ->join('store_table', 'item_price.store_id', '=', 'store_table.store_id')
            ->where(function($q){
                $q->where('ip_end_date', '>' , date('Y-m-d'))
                    ->orWhere('ip_end_date', null);
            })
            ->select('item_master.upc_ean', 'item_master.description', 'prm_store_number', 'ip_unit_price', 'descriptive_size', 'ip_start_date', 'ip_end_date')
            ->get();

        $file = View::make('pricing.export', ['products' => $products]);

        Storage::disk('s3')->put('retalix-pricing/' . date('Ymd') . '/retalix-pricing.csv', $file);

        return $products->count();
    }
}