<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GotController extends Controller
{
    /**
     *  Daily Pricing
     *
     *
     */

    public function daily_pricing($store)
    {
        try
        {
            $items = DB::select('
          
          SELECT TOP 5000
            UPC_EAN,
            DESCRIPTION,
            SELL_SIZE,
            DESCRIPTIVE_SIZE,
            STORE_POS_DEPARTMENT,
            VI_BLOCK_FROM_POS,
            CASE_COST,
            CASE_PACK,
            IP_UNIT_PRICE,
            ITEM_PRICE.PT_TYPE,
            IP_START_DATE,
            IP_END_DATE,
            ITEM_PRICE.PRICE_STRATEGY
          FROM 
            ITEM_PRICE
          JOIN
            ITEM_MASTER
          ON
            ITEM_PRICE.ITEM_ID = ITEM_MASTER.ITEM_ID
          JOIN
            VENDOR_ITEM
          ON
            ITEM_MASTER.ITEM_ID = VENDOR_ITEM.ITEM_ID
          JOIN
            VENDOR_COST
          ON
            VENDOR_ITEM.VI_ID = VENDOR_COST.VI_ID
          JOIN
            STORE_TABLE
          ON  
            ITEM_PRICE.STORE_ID = STORE_TABLE.STORE_ID
          WHERE
            ( (ITEM_PRICE.IP_END_DATE >= GetDate()) OR (ITEM_PRICE.IP_END_DATE = NULL) )
          AND 
            STORE_TABLE.PRM_STORE_NUMBER = ' . $store . '
        ');

            return view('got.daily-pricing-table', compact('items'));
//            return view('got.daily-pricing', compact('items'));
        }

        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}
