@component('mail::message')
<p>Hi,</p>
<p>This is a confirmation that the file {{ $filename }} that contains invoices information has been transferred to GOT Systems FTP Server.</p>
<p>A copy of the file is attached.</p>
@component('mail::panel')
The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.
@endcomponent
Thanks </br> {{ config('app.name') }}
@endcomponent