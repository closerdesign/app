@foreach( $items as $item )
    <!-- UPC -->(1){{ $item->UPC_EAN }}
    <!-- Vendor -->(72)
    <!-- Status -->ST(73)
    <!-- Description -->(4){{ $item->DESCRIPTION }}
    <!-- Pack -->(5){{ $item->SELL_SIZE }}
    <!-- Size -->(6){{ $item->DESCRIPTIVE_SIZE }}
    <!-- PosDescription -->(7){{ $item->DESCRIPTION }}
    <!-- Department -->(8){{ $item->STORE_POS_DEPARTMENT }}
    <!-- POS Block -->(29)@if($item->VI_BLOCK_FROM_POS == 1){{ 'Y' }}@else{{ 'N' }}@endif
    <!-- Cost -->(31){{ $item->CASE_COST / $item->CASE_PACK }}
    <!-- Price -->(34)@if( $item->PT_TYPE == 1 ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif
    <!-- Future Price -->(55)
    <!-- Future Start Date -->(51)
    <!-- TPR -->(36)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif
    <!-- TPR Start Date -->(37)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ $item->IP_START_DATE }}@endif
    <!-- TPR End Date -->(38)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ $item->IP_END_DATE }}@endif
    <!-- AD -->(47)@if( $item->PT_TYPE == 4 ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif
    <!-- AD Start Date -->(48)@if( $item->PT_TYPE == 4 ){{ $item->IP_START_DATE }}@endif
    <!-- AD End Date -->(49)@if( $item->PT_TYPE == 4 ){{ $item->IP_END_DATE }}@endif
    <!-- PriceType -->(28){{ $item->PT_TYPE }}
    <br />
    @endforeach