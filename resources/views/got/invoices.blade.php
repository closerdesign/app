Invoice_Number,Receipt_Date,Vendor_id,Store_ID,Department,UPC,Description,Cases,Units,Size,Item_ID,Delivery_Date
@foreach($invoices as $invoice)
{{ $invoice->rcv_invoice_no }},{{ $invoice->rcv_effective_date }},{{ $invoice->vendor }},{{ $invoice->prm_store_number }},{{ $invoice->store_pos_department }},{{ $invoice->rd_upc_ean }},{{ $invoice->description }},{{ $invoice->cases }},{{ $invoice->units }},{{ $invoice->sell_size }},{{ $invoice->vendor_item }},{{ $invoice->rcv_delivery_date }}
@endforeach