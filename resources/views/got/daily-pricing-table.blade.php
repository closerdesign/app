<table border="1">
    <tr>
        <th>UPC</th>
        <th>VENDOR</th>
        <th>STATUS</th>
        <th>DESCRIPTION</th>
        <th>PACK</th>
        <th>SIZE</th>
        <th>POSDESCRIPTION</th>
        <th>DEPARTMENT</th>
        <th>POS BLOCK</th>
        <th>COST</th>
        <th>PRICE</th>
        <th>FUTURE PRICE</th>
        <th>FUTURE START DATE</th>
        <th>TPR</th>
        <th>TPR START DATE</th>
        <th>TPR END DATE</th>
        <th>AD</th>
        <th>AD START DATE</th>
        <th>AD END DATE</th>
        <th>PRICE TYPE</th>
    </tr>
    @foreach( $items as $item )
    <tr>
        <td><!-- UPC -->(1){{ $item->UPC_EAN }}</td>
        <td><!-- Vendor -->(72)</td>
        <td><!-- Status -->ST(73)</td>
        <td><!-- Description -->(4){{ $item->DESCRIPTION }}</td>
        <td><!-- Pack -->(5){{ $item->SELL_SIZE }}</td>
        <td><!-- Size -->(6){{ $item->DESCRIPTIVE_SIZE }}</td>
        <td><!-- PosDescription -->(7){{ $item->DESCRIPTION }}</td>
        <td><!-- Department -->(8){{ $item->STORE_POS_DEPARTMENT }}</td>
        <td><!-- POS Block -->(29)@if($item->VI_BLOCK_FROM_POS == 1){{ 'Y' }}@else{{ 'N' }}@endif</td>
        <td><!-- Cost -->(31){{ $item->CASE_COST / $item->CASE_PACK }}</td>
        <td><!-- Price -->(34)@if( $item->PT_TYPE == 1 ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif</td>
        <td><!-- Future Price -->(55)</td>
        <td><!-- Future Start Date -->(51)</td>
        <td><!-- TPR -->(36)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif</td>
        <td><!-- TPR Start Date -->(37)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ $item->IP_START_DATE }}@endif</td>
        <td><!-- TPR End Date -->(38)@if( ($item->PT_TYPE == 2) || ($item->PT_TYPE == 15) ){{ $item->IP_END_DATE }}@endif</td>
        <td><!-- AD -->(47)@if( $item->PT_TYPE == 4 ){{ number_format($item->IP_UNIT_PRICE, 2) }}@endif</td>
        <td><!-- AD Start Date -->(48)@if( $item->PT_TYPE == 4 ){{ $item->IP_START_DATE }}@endif</td>
        <td><!-- AD End Date -->(49)@if( $item->PT_TYPE == 4 ){{ $item->IP_END_DATE }}@endif</td>
        <td><!-- PriceType -->(28){{ $item->PT_TYPE }}</td>
    </tr>
    @endforeach
</table>
