@component('mail::message')
    <p> Hi, </p>
    <p> This is a notification that the file {{ $filename }} that contains invoices information has been sent to GOT Systems FTP server.</p>
    <p>We would like to thank you for your purchase today.</p>
    <p>A copy of the file is attached.</p>
    @component('mail::panel')
        Confidential.
    @endcomponent
    Thanks </br> {{ config(‘app.name’) }}
@endcomponent