@component('mail::message')
    <p>This is a notification that the SIL Files have been downloaded from the AWG Servers and put into our folders. You can find a copy of the files attached to this message.</p>
@endcomponent