@component('mail::message')

<p>Please check the information below:</p>

@foreach( $uploads as $upload )
<p>
    <b>Store:</b> {{ $upload['store'] }}<br />
    <b>Product Count:</b> {{ $upload['data']['count'] }}<br />
    <b>Datafile:</b> {{ Storage::disk('s3')->url('online-shopping-files/' . $upload['data']['filename']) }}
</p>
@endforeach

@endcomponent