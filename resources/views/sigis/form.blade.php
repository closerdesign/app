@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                SIGIS File Import
            </div>
            <div class="card-body">
                <form action="{{ action('SigisController@process') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success btn-block">
                        Upload
                    </button>
                </form>
            </div>
        </div>
    </div>

    @endsection