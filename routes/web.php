<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Route::get('got/daily-pricing/{store}', 'GotController@daily_pricing');

Route::get('online-shopping/stores', 'OnlineShoppingController@stores');
Route::get('online-shopping/pricing/{store}', 'OnlineShoppingController@pricing');
Route::get('online-shopping/generate', 'OnlineShoppingController@generate')->name('generate');

Route::get('pricing/cloud-transfer', 'PricingController@cloud_transfer');
Route::get('pricing/export', 'PricingController@export');

Route::get('sigis', 'SigisController@form');
Route::post('sigis', 'SigisController@process');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');