<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sqlsrv'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'sales' => [
            'driver' => 'mysql',
            'host' => env('DB_SALES_HOST'),
            'port' => env('DB_SALES_PORT', '3306'),
            'database' => env('DB_SALES_DATABASE', 'forge'),
            'username' => env('DB_SALES_USERNAME', 'forge'),
            'password' => env('DB_SALES_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : []
        ],

        'sara' => [
            'driver' => 'mysql',
            'host' => env('DB_SARA_HOST'),
            'port' => env('DB_SARA_PORT', '3306'),
            'database' => env('DB_SARA_DATABASE', 'forge'),
            'username' => env('DB_SARA_USERNAME', 'forge'),
            'password' => env('DB_SARA_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : []
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'adc' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_ADC_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => 'Interstore_test_2019',
            'username' => env('DB_ADC_USERNAME', 'forge'),
            'password' => env('DB_ADC_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '3501mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('3501mfs1_HOST', 'localhost'),
            'port' => env('3501mfs1_PORT', '1433'),
            'database' => env('3501mfs1_DATABASE'),
            'username' => env('3501mfs1_USERNAME', 'forge'),
            'password' => env('3501mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '1006mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('1006mfs1_HOST', 'localhost'),
            'port' => env('1006mfs1_PORT', '1433'),
            'database' => env('1006mfs1_DATABASE'),
            'username' => env('1006mfs1_USERNAME', 'forge'),
            'password' => env('1006mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '1205mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('1205mfs1_HOST', 'localhost'),
            'port' => env('1205mfs1_PORT', '1433'),
            'database' => env('1205mfs1_DATABASE'),
            'username' => env('1205mfs1_USERNAME', 'forge'),
            'password' => env('1205mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '1201mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('1201mfs1_HOST', 'localhost'),
            'port' => env('1201mfs1_PORT', '1433'),
            'database' => env('1201mfs1_DATABASE'),
            'username' => env('1201mfs1_USERNAME', 'forge'),
            'password' => env('1201mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '7957mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('7957mfs1_HOST', 'localhost'),
            'port' => env('7957mfs1_PORT', '1433'),
            'database' => env('7957mfs1_DATABASE'),
            'username' => env('7957mfs1_USERNAME', 'forge'),
            'password' => env('7957mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '3713mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('3713mfs1_HOST', 'localhost'),
            'port' => env('3713mfs1_PORT', '1433'),
            'database' => env('3713mfs1_DATABASE'),
            'username' => env('3713mfs1_USERNAME', 'forge'),
            'password' => env('3713mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '2701mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('2701mfs1_HOST', 'localhost'),
            'port' => env('2701mfs1_PORT', '1433'),
            'database' => env('2701mfs1_DATABASE'),
            'username' => env('2701mfs1_USERNAME', 'forge'),
            'password' => env('2701mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '1230mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('1230mfs1_HOST', 'localhost'),
            'port' => env('1230mfs1_PORT', '1433'),
            'database' => env('1230mfs1_DATABASE'),
            'username' => env('1230mfs1_USERNAME', 'forge'),
            'password' => env('1230mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '9515mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('9515mfs1_HOST', 'localhost'),
            'port' => env('9515mfs1_PORT', '1433'),
            'database' => env('9515mfs1_DATABASE'),
            'username' => env('9515mfs1_USERNAME', 'forge'),
            'password' => env('9515mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '4424mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('4424mfs1_HOST', 'localhost'),
            'port' => env('4424mfs1_PORT', '1433'),
            'database' => env('4424mfs1_DATABASE'),
            'username' => env('4424mfs1_USERNAME', 'forge'),
            'password' => env('4424mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '4150mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('4150mfs1_HOST', 'localhost'),
            'port' => env('4150mfs1_PORT', '1433'),
            'database' => env('4150mfs1_DATABASE'),
            'username' => env('4150mfs1_USERNAME', 'forge'),
            'password' => env('4150mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        '952mfs1' => [
            'driver' => 'sqlsrv',
            'host' => env('952mfs1_HOST', 'localhost'),
            'port' => env('952mfs1_PORT', '1433'),
            'database' => env('952mfs1_DATABASE'),
            'username' => env('952mfs1_USERNAME', 'forge'),
            'password' => env('952mfs1_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],





    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'predis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'predis'),
        ],

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];
