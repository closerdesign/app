<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('S3_KEY'),
            'secret' => env('S3_SECRET'),
            'region' => env('S3_REGION'),
            'bucket' => env('S3_BUCKET'),
        ],

        'spins-s3' => [
            'driver' => 's3',
            'key' => env('SPINS_S3_KEY'),
            'secret' => env('SPINS_S3_SECRET'),
            'region' => env('SPINS_S3_REGION'),
            'bucket' => env('SPINS_S3_BUCKET'),
        ],

        'ftpsite' => [
            'driver' => 'local',
            'root' => 'C:\inetpub\ftproot',
        ],

        'xmlsite' => [
            'driver' => 'local',
            'root' => 'C:\Program Files\tci\RetailSuite\HQPM\HostTran\Export',
        ],

        'cloudsite' => [
            'driver' => 'sftp',
            'host' => '159.89.148.243',
            'username' => 'buyforlessok',
            'password' => 'C@mbiar123buyf',
            'port' => 22,
            'timeout' => 600
        ],
        'sara' => [
            'driver' => 'sftp',
            'host' => '142.93.126.131',
            'username' => 'retalix',
            'password' => '2tTZfQBJrprjnHm8',
            'port' => 22,
            'timeout' => 600
        ],
        'awg' => [
            'driver' => 'ftp',
            'host' => 'awgftp.awginc.com',
            'username' => 'we444000',
            'password' => 'cxf5qmwh',
            'port' => 21,
            'timeout' => 600,
            'root'   => '//we445000/',
        ],

        'hqpm' => [
            'driver' => 'local',
            'root' => 'C:\Program Files\tci\RetailSuite\HQPM\HostTran\Import',
        ],

        'got' => [
            'driver' => 'ftp',
            'host' => '198.181.250.108',
            'username' => 'buy4less_ftp@gotnet',
            'password' => 'bUy4l3s$',
            'port' => 21,
            'timeout' => 600,
            'root'   => '/BuyForLess',
        ],

        'spins-sftp' => [
            'driver' => 'sftp',
            'host' => env('SPINS_SFTP_HOST'),
            'username' => env('SPINS_SFTP_USERNAME'),
            'password' => env('SPINS_SFTP_PASSWORD'),
        ],

        'spins' => [
            'driver' => 'local',
            'root' => 'C:\Program Files\JRS\Category Analyzer\spins',
        ],

        'disks' => [

            'dropbox' => [
                'driver' => 'dropbox',
                'token'  => env('DROPBOX_TOKEN'),
            ],

        ],
    ],

];
